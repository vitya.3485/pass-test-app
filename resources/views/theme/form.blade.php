<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Theme form</title>
        <link rel="stylesheet" href="{{ asset('css/theme/form.css') }}">
    </head>
    <body>
        <div class="container">
            <h2>Theme Form</h2>
            <form method="POST" action="{{ url('test/theme/save') }}">
                @csrf
                @error('name')
                    <div class="error">{{ $message }}</div>
                @enderror
                <input type="hidden" name="id" value="{{ $theme->id ?? '' }}">
                <input type="text" name="name" placeholder="Theme name" value="{{ $theme->name ?? '' }}">
                <input type="submit" value="Send">
            </form>
        </div>
    </body>
</html>
