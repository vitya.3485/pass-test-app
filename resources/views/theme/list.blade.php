<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Theme List</title>
        <link rel="stylesheet" href="{{ asset('css/theme/list.css') }}">
    </head>
    <body>
        <div class="container">
            <h2>Theme List</h2>
            <a href="{{ url('test/theme/form') }}" class="add-button">Add New Theme</a>
            <div class="themes">
                @foreach ($themes as $theme)
                    <div class="theme">
                        <span class="name">{{ $theme->name }}</span>
                        <span class="link">
                            <a href="{{ url('test/question', ['id' => $theme->id]) }}">Questions</a>
                        </span>
                        <span class="link">
                            <a href="{{ url('test/theme/form', ['id' => $theme->id]) }}">Edit</a>
                        </span>
                        <span class="link">
                            <a href="{{ url('test/theme/delete', ['id' => $theme->id]) }}">Delete</a>
                        </span>
                    </div>
                @endforeach
            </div>
        </div>
    </body>
</html>
