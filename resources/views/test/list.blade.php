<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
    <link rel="stylesheet" href="{{ asset('css/test/list.css') }}">
</head>
    <body>
        <div class="container">
            <h2>Test</h2>
            <div class="question-label">Mark: {{ $mark }}</div>
            <form method="POST" action="{{ url('') }}">
                @csrf
                <div class="question-container">
                    @foreach($questions as $question)
                        <div class="question">
                            <div class="question-label">{{ $question->question }}</div>
                            @foreach($question->answers as $answer)
                                <div>
                                    <input type="radio"
                                           id="{{ $answer->id . '_answer' }}"
                                           name="answer[{{ $answer->test_question_id }}]"
                                           value="{{ $answer->id }}" />
                                    <label for="{{ $answer->id . '_answer' }}">
                                        {{ $answer->label }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
                <input type="submit" value="Send">
            </form>
        </div>
    </body>
</html>
