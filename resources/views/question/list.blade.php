<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Question List</title>
    <link rel="stylesheet" href="{{ asset('css/theme/list.css') }}">
</head>
<body>
<div class="container">
    <h2>Question List</h2>
    <a href="{{ url('test/question/form', $id) }}" class="add-button">Add New Question</a>
    <a href="{{ url('test/theme') }}" class="add-button">Go Back</a>
    <div class="themes">
        @foreach ($questions as $question)
            <div class="theme">
                <span class="name">{{ $question->question }}</span>
                <span class="link">
                    <a href="{{ url('test/question/form', [
                        'themeId' => $question->test_theme_id,
                        'questionId' => $question->id,
                    ]) }}">Edit</a>
                </span>
                <span class="link">
                    <a href="{{ url('test/question/delete', ['questionId' => $question->id]) }}">Delete</a>
                </span>
            </div>
        @endforeach
    </div>
</div>
</body>
</html>

