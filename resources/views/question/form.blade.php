<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Question Form</title>
        <link rel="stylesheet" href="{{ asset('css/question/form.css') }}">
    </head>
    <body>
        <div class="container">
            <h2>Question Form</h2>
            <form id="question-form" method="POST" action="{{ url('test/question/save') }}">
                @csrf
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="error">{{ $error }}</div>
                    @endforeach
                @endif
                <input type="hidden" name="id" value="{{ $question ? $question->id : '' }}">
                <input type="hidden" name="test_theme_id" value="{{ $themeId ?? '' }}">
                <div class="form-group">
                    <input id="question"
                           type="text"
                           name="question"
                           placeholder="Question Text"
                           value="{{ $question->question ?? '' }}">
                </div>
                <div class="answers" id="answers">
                    @if (!empty($question) && $question->answers->isNotEmpty())
                        @foreach ($question->answers as $key => $answer)
                            <div class="form-group answer">
                                <input type="radio"
                                       name="correct"
                                       value="{{ $key }}"
                                       {{ !$answer->is_correct ?: 'checked'  }}
                                       required>
                                <input class="answer-item"
                                       type="text"
                                       name="answers[]"
                                       placeholder="Answer"
                                       value="{{ $answer->label }}"
                                       required>
                                <button class="remove-button" type="button" onclick="removeAnswer(this)">Remove</button>
                            </div>
                        @endforeach
                    @else
                        <div class="form-group answer">
                            <input type="radio" name="correct" value="0" required>
                            <input class="answer-item" type="text" name="answers[]" placeholder="Answer" required>
                            <button class="remove-button" type="button" onclick="removeAnswer(this)">Remove</button>
                        </div>
                    @endif
                </div>
                <button type="button" class="add-answer" onclick="addAnswer()">Add Answer</button>
                <button type="submit" class="submit-btn">Submit</button>
            </form>
        </div>

        <script src="{{ asset('js/question/form.js') }}"></script>
    </body>
</html>
