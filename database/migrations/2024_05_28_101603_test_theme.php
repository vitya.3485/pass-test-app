<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('test_theme', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });

        Schema::create('test_question', function (Blueprint $table) {
            $table->id();
            $table->text('question');
            $table->unsignedBigInteger('test_theme_id');
            $table->foreign('test_theme_id')
                ->references('id')
                ->on('test_theme')
                ->onDelete('cascade');
        });

        Schema::create('test_question_option', function (Blueprint $table) {
            $table->id();
            $table->text('label');
            $table->boolean('is_correct');
            $table->unsignedBigInteger('test_question_id');
            $table->foreign('test_question_id')
                ->references('id')
                ->on('test_question')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::drop('test_theme');
        Schema::drop('test_question');
        Schema::drop('test_question_option');
    }
};
