// minimal and maximal amount of answers
const min = 1;
const max = 3;

// add event handler functions
function addAnswer() {
    if (document.querySelectorAll('.answer').length == max) return

    const answersDiv = document.getElementById('answers');
    const answerCount = answersDiv.children.length;
    const answerDiv = document.createElement('div');
    answerDiv.className = 'form-group answer';
    answerDiv.innerHTML = `
                <input type="radio" name="correct" value="${answerCount}" required>
                <input class="answer-item" type="text" name="answers[]" placeholder="Answer" required>
                <button type="button" onclick="removeAnswer(this)">Remove</button>
                `;
    answersDiv.appendChild(answerDiv);
}

function removeAnswer(button) {
    if (document.querySelectorAll('.answer').length == min) return;

    const answerDiv = button.parentNode;
    answerDiv.remove();
    updateRadioValues();
}

function updateRadioValues() {
    const answers = document.querySelectorAll('.answers .answer input[type="radio"]');
    answers.forEach((radio, index) => {
        radio.value = index;
    });
}
