<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Question;
use App\Http\Controllers\Theme;
use App\Http\Controllers\Test;

Route::prefix('test')->group(function () {
    Route::prefix('theme')->group(function () {
        Route::controller(Theme::class)->group(function () {
            Route::get('', 'showThemes');
            Route::get('form/{id?}', 'form');
            Route::get('delete/{id}', 'delete');
            Route::post('save', 'save');
        });
    });

    Route::prefix('question')->group(function () {
        Route::controller(Question::class)->group(function () {
            Route::post('save', 'save');
            Route::get('form/{themeId}/{questionId?}', 'form');
            Route::get('delete/{questionId}', 'delete');
            Route::get('{id}', 'showQuestions');
        });
    });
});

Route::get('/', [Test::class, 'index']);
Route::post('/', [Test::class, 'index']);
