<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\Theme as ThemeRequest;
use App\Models\ThemeModel;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class Theme extends Controller
{
    /**
     * Display all themes with link to update
     */
    public function showThemes(): View
    {
        return view('theme.list', ['themes' => ThemeModel::all()]);
    }

    /**
     * Display form for update or create
     */
    public function form(string $id = ''): View
    {
        return view('theme.form', ['theme' => ThemeModel::query()->find($id)]);
    }

    /**
     * Save or update theme
     */
    public function save(ThemeRequest $request): RedirectResponse
    {
        $data = $request->validated();

        if (empty($data['id'])) {
            ThemeModel::query()->create($data);
        } else {
            ThemeModel::query()->find($data['id'])->update($data);
        }

        return redirect('/test/theme/');
    }

    /**
     * Delete theme
     */
    public function delete(string $id = ''): RedirectResponse
    {
        if (!empty($id)) {
            ThemeModel::destroy($id);
        }

        return redirect('/test/theme/');
    }
}
