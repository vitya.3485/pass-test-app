<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\QuestionModel;
use App\Models\QuestionOptionModel;
use Illuminate\Http\Request;
use Illuminate\View\View;

class Test extends Controller
{
    private const QUESTION_LIMIT = 25;

    /**
     * Function to show random test questions
     */
    public function index(Request $request): View
    {
        $questions = QuestionModel::all()
            ->shuffle()
            ->slice(0, self::QUESTION_LIMIT);

        $mark = QuestionOptionModel::query()
            ->whereIn('id', $request->post('answer') ?? [])
            ->where('is_correct', 1);

        return view('test.list', [
            'questions' => $questions,
            'mark'      => $mark->count() . '/' . $questions->count(),
        ]);
    }
}
