<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\QuestionModel;
use App\Models\QuestionOptionModel;
use App\Http\Requests\Question as RequestQuestion;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class Question extends Controller
{
    /**
     * Show all theme questions
     */
    public function showQuestions(string $id): View
    {
        if (empty($id)) {
            return redirect('/test/theme');
        }

        $questions = QuestionModel::all()->where('test_theme_id', $id);

        return view('question.list', ['id' => $id, 'questions' => $questions]);
    }

    /**
     * Display form for create or update
     */
    public function form(string $themeId = '', string $questionId = ''): View|RedirectResponse
    {
        if (empty($themeId)) {
            return redirect('/test/theme');
        }

        $question = null;
        if (!empty($questionId)) {
            $question = QuestionModel::query()->find($questionId);
        } else {
            url('/test/question', ['test_theme_id' => $themeId]);
        }

        return view('question.form', ['themeId' => $themeId, 'question' => $question]);
    }

    /**
     * Save or update question
     */
    public function save(RequestQuestion $request): RedirectResponse
    {
        $data = $request->validated();
        DB::beginTransaction();

        try {
            if (empty($data['id'])) {
                $question = QuestionModel::query()->create($data);
            } else {
                $question = QuestionModel::query()->find($data['id']);
                $question->update($data);
                QuestionOptionModel::query()->where('test_question_id', $data['id'])->delete();
            }

            foreach ($data['answers'] as $key => $questionLabel) {
                QuestionOptionModel::query()->create([
                    'label' => $questionLabel,
                    'is_correct' => $key == $data['correct'] ? 1 : 0,
                    'test_question_id' => $question->id,
                ]);
            }

            DB::commit();
        } catch (\Exception) {
            DB::rollBack();
        }

        return redirect(url('/test/question', ['test_theme_id' => $data['test_theme_id']]));
    }

    /**
     * Delete question
     */
    public function delete(string $id): RedirectResponse
    {
        if (!empty($id)) {
            QuestionModel::destroy($id);
        }

        return back();
    }
}
