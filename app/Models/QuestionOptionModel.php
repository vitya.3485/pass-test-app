<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionOptionModel extends Model
{
    use HasFactory;
    protected $table = 'test_question_option';
    protected $fillable = ['label', 'is_correct', 'test_question_id'];
    public $timestamps = false;
}
