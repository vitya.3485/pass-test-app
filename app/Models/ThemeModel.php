<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ThemeModel extends Model
{
    use HasFactory;

    protected $table = 'test_theme';
    protected $fillable = ['name'];
    public $timestamps = false;
}
