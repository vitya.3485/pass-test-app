<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class QuestionModel extends Model
{
    use HasFactory;
    protected $table = 'test_question';
    protected $fillable = ['question', 'test_theme_id'];
    public $timestamps = false;

    /**
     * Join answers
     */
    public function answers(): HasMany
    {
        return $this->hasMany(QuestionOptionModel::class, 'test_question_id');
    }
}
